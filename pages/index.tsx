import Head from "next/head";
import styled from "styled-components";
import { SiteLayout } from "@oua/web-components";
const { Layout, Footer, Main, Container, Header } = SiteLayout;

const Title = styled.h1`
  font-size: 50px;
  color: ${({ theme }) => theme.colors.primary};
`;

interface Provider {
  providerCode: {
    value: string;
  };
  providerLogo: {
    src: string;
  };
  onClickSearchURL: {
    url: string;
  };
}
interface HomeProps {
  providers: [Provider];
}

export default function Home({ providers, content, visitor }: HomeProps) {
  const basketCount = visitor.basket?.items?.length ?? 0;
  const favouriteCount = visitor.favourites?.items?.length ?? 0;
  return (
    <Layout>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header
        content={headerContent.content}
        basket={<span>{basketCount}</span>}
        favourites={<span>{favouriteCount}</span>}
      />
      <Main>
        {providers.map((p) => {
          return (
            <a
              href={`https://www.open.edu.au${p.onClickSearchURL.url}`}
              key={p.providerCode.value}
            >
              {p.providerCode.value}
              <img src={`https://www.open.edu.au${p.providerLogo.src}`} />
            </a>
          );
        })}
      </Main>
      <Footer content={content.footer} />
    </Layout>
  );
}

export async function getStaticProps(context) {
  const data = await getSitecoreData();

  return {
    props: {
      visitor: data.visitor,
      content: {
        footer: data.content.footer,
      },
      providers: data.providerComponent.providers,
    }, // will be passed to the page component as props
  };
}

async function fetchAPI(query) {
  const body = JSON.stringify({
    query,
  });

  const res = await fetch(
    "https://sitecore-jss.sc9.env.opendev.edu.au/sitecore/api/graph/items/web/?sc_apikey=%7BF04D1DF0-2FEA-4E47-B3DB-31C2268B2752%7D",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body,
    }
  );
  const json = await res.json();

  if (json.errors) {
    console.log(process.env.NEXT_EXAMPLE_CMS_GCMS_PROJECT_ID);
    console.error(json.errors);
    throw new Error("Failed to fetch API");
  }

  return json.data;
}

export async function getSitecoreData() {
  const data = await fetchAPI(
    `{
      visitor {
        username
        fullname
        isAuthenticated
        favourites {
          items {
            ouaCode
          }
        }
        basket {
          items {
            name
            ouaCode
          }
        }
      }
      content {
        footer {
          copytext
          image {
            name
            src
          }
          copyLinks {
            url
            name
          }
          links {
            url
            name
          }
          socialLinks {
            name
            url
          }
        }
      }
      providerComponent:item(path: "/sitecore/content/Alchemy/Global/Home Contents/ProvidersComponent") {
        providers:children {
          ... on ProvidersComponent {
            providerCode {
              value
            }
            providerLogo {
              ... on ImageField {
                src(maxWidth: 100)
              }
            }
            onClickSearchURL {
              ... on LinkField {
                url
              }
            }
          }
        }
      }
    }
    `
  );
  return data;
}

export const headerContent = {
  content: {
    image: {
      id: "1",
      name: "Open Universities Australia",
      url:
        "https://www.open.edu.au/-/media/project/oua/logo/open-universities-australia.svg?h=50&iar=0&mh=50&w=149&sc_lang=en&revision=3a8a5887-5bad-450a-ae90-be394ae1a740&hash=925D78ACFB84A9E538F31205E3F6BE1A",
    },
    headerLinks: [
      {
        id: "1",
        url: "/study-online",
        name: "Study online",
      },
      {
        id: "2",
        url: "/How OUA Works",
        name: "How OUA Works",
      },
      {
        id: "3",
        url: "/your-studies",
        name: "Your Studies",
      },
      {
        id: "4",
        url: "/advice",
        name: "Advice",
      },
    ],
  },
};
